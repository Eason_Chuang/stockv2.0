﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Enum
{
    public enum Market
    {
        /// <summary>
        /// 證交所
        /// </summary>
        TWS = 0, 

        /// <summary>
        /// 櫃買中心
        /// </summary>
        TWE = 1,
    }
}

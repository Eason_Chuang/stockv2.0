﻿using Stock.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Error
{
    public class Error
    {
        public static void Log(ErrorType _errortype, string _error) {
            // Set a variable to the Documents path.
            string docPath = $"C:/Log";

            bool folderExists = Directory.Exists(docPath);
            if (!folderExists)
                Directory.CreateDirectory(docPath);

            // Append text to an existing file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, $"{_errortype.ToString()}.txt"), true))
            {
                outputFile.WriteLine(_error);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model.DB
{
    public class TWEThree
    {

        public DateTime Date { get; set; }
        public string Stock { get; set; }
        public string Name { get; set; }
        public string CB { get; set; }
        public string CS { get; set; }
        public string CT { get; set; }
        public string CLB { get; set; }
        public string CLS { get; set; }
        public string CLT { get; set; }
        public string HB { get; set; }
        public string HS { get; set; }
        public string HT { get; set; }
        public string MB { get; set; }
        public string MS { get; set; }
        public string MT { get; set; }
        public string LSB { get; set; }
        public string LSS { get; set; }
        public string LST { get; set; }
        public string LHB { get; set; }
        public string LHS { get; set; }
        public string LHT { get; set; }
        public string LB { get; set; }
        public string LS { get; set; }
        public string LT { get; set; }
        public string T { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model.DB
{
    public class TWEMain
    {
        public DateTime Date { get; set; }
        public string Stock { get; set; }
        public string Name { get; set; }
        public string Close { get; set; }
        public string Spread { get; set; }
        public string Open { get; set; }
        public string High { get; set; }
        public string Low { get; set; }
        public string AvgPrice { get; set; }
        public string DealQty { get; set; }
        public string DealPrice { get; set; }
        public string DealCount { get; set; }
        public string BP { get; set; }
        public string SP { get; set; }
        public string IssueStock { get; set; }
        public string TP { get; set; }
        public string TUP { get; set; }
        public string TDOWN { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class ThreeBS
    {
        public string stat { get; set; }

        public string date { get; set; }

        public string title { get; set; }
        public IEnumerable<string> fields{get;set;}

        public IEnumerable<IEnumerable<string>> data { get; set; }

        public string selectType { get; set; }

        public IEnumerable<string> notes { get; set; }
    }
}

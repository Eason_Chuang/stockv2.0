﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class MainBS_TWE
    {
        public string reportDate { get; set; }
        public string reportTitle { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public int listNum { get; set; }
        public IEnumerable<IEnumerable<string>> aaData { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class MainBS
    {
        public string date { get; set; }
        public string stat { get; set; }
        public IEnumerable<IEnumerable<string>> data9 { get; set; }
        public IEnumerable<string> fields9 { get; set; }
        public IEnumerable<IEnumerable<string>> data8 { get; set; }
        public IEnumerable<string> fields8 { get; set; }
    }
}

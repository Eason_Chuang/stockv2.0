﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class StockThree
    {
        public string StockCode { get; set; }
        public string StockName { get; set; }
        public DateTime Date { get; set; }
        public Int64? H_B { get; set; }
        public Int64? H_S { get; set; }
        public Int64? H_T { get; set; }
        public Int64? M_B { get; set; }
        public Int64? M_S { get; set; }
        public Int64? M_T { get; set; }
        public Int64? L_B { get; set; }
        public Int64? L_S { get; set; }
        public Int64? L_T { get; set; }
        public Int64? LA_B { get; set; }
        public Int64? LA_S { get; set; }
        public Int64? LA_T { get; set; }
        public Int64? T { get; set; }
    }
}

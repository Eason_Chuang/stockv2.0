﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class StockQty
    {
        public string StockCode { get; set; }
        public string StockName { get; set; }
        public DateTime Date { get; set; }
        public Int64 DealQty { get; set; }
        public Int64 DealPrice { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public string Sign { get; set; }
        public double Spread { get; set; }
        public Int64 DealTotal { get; set; }
        public int MarketNo { get; set; }
        public double PE { get; set; }
        public Int64? IssueStock { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Model
{
    public class Stock_PE
    {
        public string StockCode { get; set; }
        public string StockName { get; set; }
        public DateTime Date { get; set; }
        public decimal EPS { get; set; }
        public decimal Close { get; set; }
        public decimal PE { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Extension
{
    public static class JsonExtension
    {
        public static string AsJson<T>(this T val) {
            return JsonConvert.SerializeObject(val);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Extension
{
    public static class DateTimeExtension
    {
        public static string AsDate(this DateTime val) {
            return val.ToString("yyyyMMdd");
        }

        public static string AsTWDate(this DateTime val)
        {
            var twCalendar = new TaiwanCalendar();
            return $"{twCalendar.GetYear(val)}/{val.Month.ToString().PadLeft(2, '0')}/{val.Day.ToString().PadLeft(2, '0')}";
        }

        public static DateTime AsDate(this string val) {
            return DateTime.ParseExact(val, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
        }

        public static DateTime AsTWDate(this string val)
        {
            DateTime dt = DateTime.ParseExact(val, "yyy/MM/dd", CultureInfo.InvariantCulture).AddYears(1911);
            return dt;
        }
    }
}

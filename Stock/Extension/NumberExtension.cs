﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.Extension
{
    public static class NumberExtension
    {
        public static Int64 AsInt64(this string val)
        {
            var styles = NumberStyles.Integer | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
            CultureInfo provider = CultureInfo.InvariantCulture;

            Int64 output;

            return Int64.TryParse(val, styles, provider, out output) ? output : Int64.MaxValue;
        }

        public static double AsDouble(this string val) {
            var styles = NumberStyles.Integer | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
            CultureInfo provider = CultureInfo.InvariantCulture;

            double output;

            return Double.TryParse(val, styles, provider, out output) ? output : double.MaxValue;
        }
    }
}

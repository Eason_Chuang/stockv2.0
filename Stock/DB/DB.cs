﻿using Stock.Enum;
using System;
using System.Data;
using System.Data.SqlClient;
using Stock.Extension;
using System.Collections.Generic;

namespace Stock.DB
{
    public static class DB
    {
        public const string exchangeConnentString = "Data Source=220.132.227.9;Database=Exchange ;Uid=EasonSa;Pwd=aa80503736;";
        public const string stockserviceConnentString = "Data Source=220.132.227.9;Database=StockService ;Uid=EasonSa;Pwd=aa80503736;";

        public static object Read<T>()
        {
            throw new NotImplementedException();
        }

        public static int batchSize = int.MaxValue;

        //DB Table
        public static string TW_Three = "dbo.TW_Three";
        public static string TWE_Three = "dbo.TWE_Three";
        public static string TW_Main = "dbo.TW_Main";
        public static string TWE_Main = "dbo.TWE_Main";
        public static string Stock_Qty = "dbo.Stock_Qty";
        public static string Stock_Three = "dbo.Stock_Three";

        public static List<T> Read<T>(string sql, string constr = exchangeConnentString) {
            DataTable dataTable = new DataTable();

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(sql, connection);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.
                    connection.Open();

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    
                    da.Fill(dataTable);
                    da.Dispose();
                }
            }
            catch (Exception ex)
            {
                Error.Error.Log(ErrorType.DB, ex.StackTrace);
            }

            return dataTable.AsDataList<T>() ?? new List<T>();
        }

        public static void Insert(DataTable dtInsertRows, string _table, string constr = exchangeConnentString) {
            try
            {
                using (SqlBulkCopy sbc = new SqlBulkCopy(constr, SqlBulkCopyOptions.KeepIdentity))
                {
                    sbc.BulkCopyTimeout = 0;

                    sbc.DestinationTableName = _table;

                    // Number of records to be processed in one go
                    sbc.BatchSize = batchSize;

                    // Finally write to server
                    sbc.WriteToServer(dtInsertRows);
                }
            }
            catch (Exception ex) {
                Error.Error.Log(ErrorType.DB, $"Date:{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}{Environment.NewLine}Error:{ex.StackTrace}{Environment.NewLine}Data:{dtInsertRows.AsJson()}{Environment.NewLine}");
            }
        }
    }
}

﻿using Stock.DB;
using Stock.Model;
using Stock.Model.DB;
using System;
using System.Linq;
using Stock.Enum;
using System.Globalization;
using System.Collections.Generic;
using Stock.Extension;

namespace Transfer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"開始轉換資料");

            // 基本資料(證交所)
            var newMainDB = DB.Read<TWMain>(@"select 
                                                *
                                                from [Exchange].[dbo].[TW_Main] src
                                                where NOT Exists(
	                                                select 1 from StockService.dbo.Stock_Qty main where src.[Stock] = main.StockCode and src.[Date] = main.[Date]
                                                )");

            List<StockQty> oldMainDB = new List<StockQty>();

            foreach (var i in newMainDB) {
                oldMainDB.Add(new StockQty()
                {
                    StockCode = i.Stock,
                    StockName = i.Name,
                    Date = i.Date,
                    DealQty = i.DealQty.AsInt64(),
                    DealPrice = i.DealPrice.AsInt64(),
                    Open = i.Open.AsDouble(),
                    High = i.High.AsDouble(),
                    Low = i.Low.AsDouble(),
                    Close = i.Close.AsDouble(),
                    Sign = i.Sign.Contains("+") ? "+" : (i.Sign.Contains("-") ? "-" : String.Empty),
                    Spread = i.Spread.AsDouble(),
                    DealTotal = i.DealCount.AsInt64(),
                    MarketNo = (int)Market.TWS,
                    PE = i.PE.AsDouble(),
                    IssueStock = null,
                    CreateTime = DateTime.Now
                });
            }

            oldMainDB = oldMainDB.Where(i => !(i.DealQty == Int64.MaxValue || i.DealPrice == Int64.MaxValue || i.Open == double.MaxValue || i.High == double.MaxValue || i.Low == double.MaxValue || i.Close == double.MaxValue || i.Spread == double.MaxValue || i.DealTotal == Int64.MaxValue || i.PE == double.MaxValue)).ToList();
            DB.Insert(oldMainDB.ToDataTable(), DB.Stock_Qty, DB.stockserviceConnentString);

            Console.WriteLine($"基本資料(證交所)轉換完成");

            // 三大法人(證交所)
            var newThreeDB = DB.Read<TWThree>(@"select 
                                                *
                                                from [Exchange].[dbo].[TW_Three] src
                                                where NOT Exists(
	                                                select 1 from StockService.dbo.Stock_Three main where src.[Stock] = main.StockCode and src.[Date] = main.[Date]
                                                )");

            List<StockThree> oldThreeDB = new List<StockThree>();

            foreach (var i in newThreeDB) {
                oldThreeDB.Add(new StockThree()
                {
                    StockCode = i.Stock,
                    StockName = i.Name,
                    Date = i.Date,
                    H_B = i.CB.AsInt64(),
                    H_S = i.CS.AsInt64(),
                    H_T = i.CT.AsInt64(),
                    M_B = i.MB.AsInt64(),
                    M_S = i.MS.AsInt64(),
                    M_T = i.MT.AsInt64(),
                    LA_B = i.LHB.AsInt64(),
                    LA_S = i.LHS.AsInt64(),
                    LA_T = i.LHT.AsInt64(),
                    L_B = i.LSB.AsInt64(),
                    L_S = i.LSS.AsInt64(),
                    L_T = i.LST.AsInt64(),
                    T = i.T.AsInt64()
                });
            }

            oldThreeDB = oldThreeDB.Where(i => !(i.H_B == Int64.MaxValue || i.H_S == Int64.MaxValue || i.H_T == Int64.MaxValue || i.M_B == Int64.MaxValue || i.M_S == Int64.MaxValue || i.M_T == Int64.MaxValue || i.LA_B == Int64.MaxValue || i.LA_S == Int64.MaxValue || i.LA_T == Int64.MaxValue || i.L_B == Int64.MaxValue || i.L_S == Int64.MaxValue || i.T == Int64.MaxValue)).ToList();
            DB.Insert(oldThreeDB.ToDataTable(), DB.Stock_Three, DB.stockserviceConnentString);

            Console.WriteLine($"三大法人(證交所)轉換完成");

            // 基本資料(櫃買中心)
            var newMainTWEDB = DB.Read<TWEMain>(@"select 
                                                *
                                                from [Exchange].[dbo].[TWE_Main] src
                                                where NOT Exists(
	                                                select 1 from StockService.dbo.Stock_Qty main where src.[Stock] = main.StockCode and src.[Date] = main.[Date]
                                                )");

            List<StockQty> oldMainTWEDB = new List<StockQty>();
            foreach (var i in newMainTWEDB)
            {
                oldMainTWEDB.Add(new StockQty()
                {
                    StockCode = i.Stock,
                    StockName = i.Name,
                    Date = i.Date,
                    DealQty = i.DealQty.AsInt64(),
                    DealPrice = i.DealPrice.AsInt64(),
                    Open = i.Open.AsDouble(),
                    High = i.High.AsDouble(),
                    Low = i.Low.AsDouble(),
                    Close = i.Close.AsDouble(),
                    Sign = i.Spread.Contains("+") ? "+" : (i.Spread.Contains("-") ? "-" : String.Empty),
                    Spread = i.Spread.AsDouble(),
                    DealTotal = i.DealPrice.AsInt64(),
                    MarketNo = (int)Market.TWE,
                    PE = double.MaxValue,
                    IssueStock = i.IssueStock.AsInt64(),
                    CreateTime = DateTime.Now
                });
            }

            oldMainTWEDB = oldMainTWEDB.Where(i => !(i.DealQty == Int64.MaxValue || i.DealPrice == Int64.MaxValue || i.Open == double.MaxValue || i.High == double.MaxValue || i.Low == double.MaxValue || i.Close == double.MaxValue || i.Spread == double.MaxValue || i.DealTotal == Int64.MaxValue)).ToList();
            DB.Insert(oldMainTWEDB.ToDataTable(), DB.Stock_Qty, DB.stockserviceConnentString);

            Console.WriteLine($"基本資料(櫃買中心)轉換完成");

            // 三大法人(櫃買中心)
            var newThreeTWEDB = DB.Read<TWEThree>(@"select 
                                                    *
                                                    from [Exchange].[dbo].[TWE_Three] src
                                                    where NOT Exists(
	                                                    select 1 from StockService.dbo.Stock_Three main where src.[Stock] = main.StockCode and src.[Date] = main.[Date]
                                                    )");

            List<StockThree> oldThreeTWEDB = new List<StockThree>();

            foreach (var i in newThreeTWEDB)
            {
                oldThreeTWEDB.Add(new StockThree()
                {
                    StockCode = i.Stock,
                    StockName = i.Name,
                    Date = i.Date,
                    H_B = i.CB.AsInt64(),
                    H_S = i.CS.AsInt64(),
                    H_T = i.CT.AsInt64(),
                    M_B = i.MB.AsInt64(),
                    M_S = i.MS.AsInt64(),
                    M_T = i.MT.AsInt64(),
                    LA_B = i.LHB.AsInt64(),
                    LA_S = i.LHS.AsInt64(),
                    LA_T = i.LHT.AsInt64(),
                    L_B = i.LSB.AsInt64(),
                    L_S = i.LSS.AsInt64(),
                    L_T = i.LST.AsInt64(),
                    T = i.T.AsInt64()
                });
            }

            oldThreeTWEDB = oldThreeTWEDB.Where(i => !(i.H_B == Int64.MaxValue || i.H_S == Int64.MaxValue || i.H_T == Int64.MaxValue || i.M_B == Int64.MaxValue || i.M_S == Int64.MaxValue || i.M_T == Int64.MaxValue || i.LA_B == Int64.MaxValue || i.LA_S == Int64.MaxValue || i.LA_T == Int64.MaxValue || i.L_B == Int64.MaxValue || i.L_S == Int64.MaxValue || i.T == Int64.MaxValue)).ToList();
            DB.Insert(oldThreeTWEDB.ToDataTable(), DB.Stock_Three, DB.stockserviceConnentString);

            Console.WriteLine($"三大法人(櫃買中心)轉換完成");
        }
    }
}

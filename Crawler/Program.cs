﻿using Crawler.Service;
using Stock.DB;
using Stock.Error;
using Stock.Model.DB;
using System;
using System.Linq;

namespace Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime today = DateTime.Now.Date;

            Console.WriteLine($"爬蟲開始");

            //基本資料 (證交所)
            //提供日期 : 2004/2/11
            var maindate = DB.Read<TWThree>(@"SELECT TOP (1) * FROM [Exchange].[dbo].[TW_Main] with(nolock) order by [Date] desc").Select(d => d.Date).FirstOrDefault();
            maindate = maindate == new DateTime() ? new DateTime(2004, 2, 11) : maindate;

            DateTime maininsertdate = maindate;
            var mainstock = new MainStock();
            for (DateTime counter = maindate; counter <= today; counter = counter.AddDays(1))
            {
                Console.WriteLine($"證交所(基本資料) : {counter.ToString("yyyy/MM/dd")}");
                var mainstockObj = mainstock.Start(counter);
                if (mainstockObj.stat == "OK")
                {
                    mainstock.Insert(mainstockObj);
                }
            }

            //基本資料 (櫃買中心)
            //提供日期 : 2007/1/1
            var maintwedate = DB.Read<TWEMain>(@"SELECT top 1 * FROM [dbo].[TWE_Main] with(nolock) order by[Date] desc").Select(d => d.Date).FirstOrDefault();
            maintwedate = maintwedate == new DateTime() ? new DateTime(2007, 1, 1) : maintwedate;
            var mainstocktwe = new MainStock_TWE();
            for (DateTime counter = maintwedate; counter <= today; counter = counter.AddDays(1))
            {
                Console.WriteLine($"櫃買中心(基本資料) : {counter.ToString("yyyy/MM/dd")}");
                var mainTweObj = mainstocktwe.Start(counter);
                mainstocktwe.Insert(mainTweObj);
            }

            //三大法人 (證交所)
            //提供日期 : 2012/5/2
            var threedate = DB.Read<TWThree>(@"SELECT top 1 * FROM[Exchange].[dbo].[TW_Three] with(nolock) order by[Date] desc").Select(d => d.Date).FirstOrDefault();
            threedate = threedate == new DateTime() ? new DateTime(2012, 5, 2) : threedate;
            var three = new Three();
            for (DateTime counter = threedate; counter <= today; counter = counter.AddDays(1))
            {
                Console.WriteLine($"證交所(三大法人) : {counter.ToString("yyyy/MM/dd")}");
                var threeObj = three.Start(counter);
                if (threeObj.stat == "OK")
                {
                    three.Insert(threeObj);
                }
            }

            //三大法人 (櫃買中心)
            //提供日期 : 2007/4/20
            var threetwedate = DB.Read<TWEThree>(@"SELECT top 1 * FROM [dbo].[TWE_Three] with(nolock) order by[Date] desc").Select(d => d.Date).FirstOrDefault();
            threetwedate = threetwedate == new DateTime() ? new DateTime(2007, 4, 20) : threetwedate;
            var three_twe = new Three_TWE();
            for (DateTime counter = threedate; counter <= today; counter = counter.AddDays(1))
            {
                Console.WriteLine($"櫃買中心(三大法人) : {counter.ToString("yyyy/MM/dd")}");
                var threetseObj = three_twe.Start(counter);
                three_twe.Insert(threetseObj);
            }
        }
    }
}

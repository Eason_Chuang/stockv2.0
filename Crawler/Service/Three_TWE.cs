﻿using Stock.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Stock.Extension;
using Newtonsoft.Json;
using Stock.Model.DB;
using System.Linq;
using Stock.DB;
using System.Data;

namespace Crawler.Service
{
    public class Three_TWE
    {
        public string Url = ConfigurationManager.AppSettings["ThreeTWEUrl"]?.ToString();

        public ThreeBS_TWE Start(DateTime _date)
        {
            //https://www.tpex.org.tw/web/stock/3insti/daily_trade/3itrade_hedge_result.php?l=zh-tw&se=EW&t=D&d=108/08/06&_=1565353437513
            double timeStamp = DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            var url = $"{Url}?l=zh-tw&se=EW&t=D&d={_date.AsTWDate()}&_={(int)timeStamp}";
            var json = new Http().Get(url);
            return JsonConvert.DeserializeObject<ThreeBS_TWE>(json);
        }

        public void Insert(ThreeBS_TWE three)
        {
            var data = three.aaData.Select(d => d);

            List<TWEThree> db_three = new List<TWEThree>();

            foreach (var d in data)
            {
                db_three.Add(new TWEThree()
                {
                    Date = three.reportDate.AsTWDate(),
                    Stock = d.ToArray()[0],
                    Name = d.ToArray()[1],
                    CB = d.ToArray()[2],
                    CS = d.ToArray()[3],
                    CT = d.ToArray()[4],
                    CLB = d.ToArray()[5],
                    CLS = d.ToArray()[6],
                    CLT = d.ToArray()[7],
                    HB = d.ToArray()[8],
                    HS = d.ToArray()[9],
                    HT = d.ToArray()[10],
                    MB = d.ToArray()[11],
                    MS = d.ToArray()[12],
                    MT = d.ToArray()[13],
                    LSB = d.ToArray()[14],
                    LSS = d.ToArray()[15],
                    LST = d.ToArray()[16],
                    LHB = d.ToArray()[17],
                    LHS = d.ToArray()[18],
                    LHT = d.ToArray()[19],
                    LB = d.ToArray()[20],
                    LS = d.ToArray()[21],
                    LT = d.ToArray()[22],
                    T = d.ToArray()[23],
                    CreateTime = DateTime.Now
                });
            }

            // Get the DataTable 
            DataTable dtInsertRows = db_three.ToDataTable();

            DB.Insert(dtInsertRows, DB.TWE_Three);

        }
    }
}

﻿using Newtonsoft.Json;
using Stock.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Stock.Extension;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Stock.Model.DB;
using Stock.DB;
using Stock.Error;

namespace Crawler.Service
{
    public class Three
    {
        public string ThreeUrl = ConfigurationManager.AppSettings["ThreeUrl"]?.ToString();

        public ThreeBS Start(DateTime _date)
        {
            //https://www.twse.com.tw/fund/T86?response=json&date=20190801&selectType=ALLBUT0999&_=1564676729202
            double timeStamp = DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            var url = $"{ThreeUrl}?response=json&date={_date.AsDate()}&selectType=ALLBUT0999&_={(int)timeStamp}";
            var json = new Http().Get(url);
            return JsonConvert.DeserializeObject<ThreeBS>(json);
        }

        public void Insert(ThreeBS three) {

            var data = three.data.Select(d => d);

            List<TWThree> db_three = new List<TWThree>();

            foreach (var d in data) {
                if (three.date.AsDate() >= new DateTime(2017, 12, 18)) {
                    db_three.Add(new TWThree()
                    {
                        Date = three.date.AsDate(),
                        Stock = d.ToArray()[0],
                        Name = d.ToArray()[1],
                        CB = d.ToArray()[2],
                        CS = d.ToArray()[3],
                        CT = d.ToArray()[4],
                        HB = d.ToArray()[5],
                        HS = d.ToArray()[6],
                        HT = d.ToArray()[7],
                        MB = d.ToArray()[8],
                        MS = d.ToArray()[9],
                        MT = d.ToArray()[10],
                        LSB = d.ToArray()[12],
                        LSS = d.ToArray()[13],
                        LST = d.ToArray()[14],
                        LHB = d.ToArray()[15],
                        LHS = d.ToArray()[16],
                        LHT = d.ToArray()[17],
                        LT = d.ToArray()[11],
                        T = d.ToArray()[11],
                        CreateTime = DateTime.Now
                    });
                }
                else if( new DateTime(2014, 12, 01) <= three.date.AsDate() && three.date.AsDate() < new DateTime(2017, 12, 18))
                {
                    db_three.Add(new TWThree()
                    {
                        Date = three.date.AsDate(),
                        Stock = d.ToArray()[0],
                        Name = d.ToArray()[1],
                        CB = string.Empty,
                        CS = string.Empty,
                        CT = string.Empty,
                        HB = d.ToArray()[2],
                        HS = d.ToArray()[3],
                        HT = d.ToArray()[4],
                        MB = d.ToArray()[5],
                        MS = d.ToArray()[6],
                        MT = d.ToArray()[7],
                        LSB = d.ToArray()[9],
                        LSS = d.ToArray()[10],
                        LST = d.ToArray()[11],
                        LHB = d.ToArray()[12],
                        LHS = d.ToArray()[13],
                        LHT = d.ToArray()[14],
                        LT = d.ToArray()[8],
                        T = d.ToArray()[15],
                        CreateTime = DateTime.Now
                    });
                }
                else
                {
                    db_three.Add(new TWThree()
                    {
                        Date = three.date.AsDate(),
                        Stock = d.ToArray()[0],
                        Name = d.ToArray()[1],
                        CB = string.Empty,
                        CS = string.Empty,
                        CT = string.Empty,
                        HB = d.ToArray()[2],
                        HS = d.ToArray()[3],
                        HT = d.ToArray()[4],
                        MB = d.ToArray()[5],
                        MS = d.ToArray()[6],
                        MT = d.ToArray()[7],
                        LSB = d.ToArray()[9],
                        LSS = d.ToArray()[10],
                        LST = d.ToArray()[8],
                        LHB = string.Empty,
                        LHS = string.Empty,
                        LHT = string.Empty,
                        LT = string.Empty,
                        T = d.ToArray()[11],
                        CreateTime = DateTime.Now
                    });

                }
            }

            // Get the DataTable 
            DataTable dtInsertRows = db_three.ToDataTable();

            DB.Insert(dtInsertRows, DB.TW_Three);
        }

    }
}

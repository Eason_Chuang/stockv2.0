﻿using Stock.Enum;
using Stock.Error;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Crawler.Service
{
    public class Http
    {
        public string Get(string URL)
        {
            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.ContentType = "application/json; charset=utf-8";
                //request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes("username:password"));
                request.PreAuthenticate = true;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex) {
                Error.Log(ErrorType.Request, $"Date:{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}{Environment.NewLine}URL: {URL}{Environment.NewLine}Error:{ex.StackTrace}{Environment.NewLine}");
                return String.Empty;
            }
        }
    }
}

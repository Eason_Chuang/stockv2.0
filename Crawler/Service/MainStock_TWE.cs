﻿using Newtonsoft.Json;
using Stock.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Stock.Extension;
using System.Linq;
using Stock.Model.DB;
using System.Data;
using Stock.DB;

namespace Crawler.Service
{
    public class MainStock_TWE
    {
        public string Url = ConfigurationManager.AppSettings["MainTWEUrl"]?.ToString();

        public MainBS_TWE Start(DateTime _date)
        {
            //https://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d=108/08/06&_=1565359935209
            double timeStamp = DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            var url = $"{Url}?l=zh-tw&d={_date.AsTWDate()}&_={(int)timeStamp}";
            var json = new Http().Get(url);
            return JsonConvert.DeserializeObject<MainBS_TWE>(json);
        }

        public void Insert(MainBS_TWE main)
        {
            var data = main.aaData.Select(d => d);

            List<TWEMain> db_main = new List<TWEMain>();

            foreach (var d in data)
            {
                db_main.Add(new TWEMain()
                {
                    Date = main.reportDate.AsTWDate(),
                    Stock = d.ToArray()[0],
                    Name = d.ToArray()[1],
                    Close = d.ToArray()[2],
                    Spread = d.ToArray()[3],
                    Open = d.ToArray()[4],
                    High = d.ToArray()[5],
                    Low = d.ToArray()[6],
                    AvgPrice = d.ToArray()[7],
                    DealQty = d.ToArray()[8],
                    DealPrice = d.ToArray()[9],
                    DealCount = d.ToArray()[10],
                    BP = d.ToArray()[11],
                    SP = d.ToArray()[12],
                    IssueStock = d.ToArray()[13],
                    TP = d.ToArray()[14],
                    TUP = d.ToArray()[15],
                    TDOWN = d.ToArray()[16],
                    CreateTime = DateTime.Now
                });
            }

            // Get the DataTable 
            DataTable dtInsertRows = db_main.ToDataTable();
            DB.Insert(dtInsertRows, DB.TWE_Main);

        }
    }
}

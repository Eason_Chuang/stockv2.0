﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Stock.DB;
using Stock.Extension;
using Stock.Model;
using Stock.Model.DB;

namespace Crawler.Service
{
    public class MainStock
    {
        public string MainUrl = ConfigurationManager.AppSettings["MainUrl"]?.ToString();

        public MainBS Start(DateTime _date)
        {
            //https://www.twse.com.tw/exchangeReport/MI_INDEX?response=json&date=20190802&type=ALLBUT0999&_=1564933134248
            double timeStamp = DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            var url = $"{MainUrl}?response=json&date={_date.AsDate()}&type=ALLBUT0999&_={(int)timeStamp}";
            var json = new Http().Get(url);
            return JsonConvert.DeserializeObject<MainBS>(json);
        }

        public void Insert(MainBS main)
        {
            var data = main.data9?.Select(d => d) ?? main.data8?.Select(d => d);

            List<TWMain> db_main = new List<TWMain>();

            foreach (var d in data) {
                db_main.Add(new TWMain()
                {
                    Date = main.date.AsDate(),
                    Stock = d.ToArray()[0],
                    Name = d.ToArray()[1],
                    DealQty = d.ToArray()[2],
                    DealCount = d.ToArray()[3],
                    DealPrice = d.ToArray()[4],
                    Open = d.ToArray()[5],
                    High = d.ToArray()[6],
                    Low = d.ToArray()[7],
                    Close = d.ToArray()[8],
                    Sign = d.ToArray()[9],
                    Spread = d.ToArray()[10],
                    BP = d.ToArray()[11],
                    BQ = d.ToArray()[12],
                    SP = d.ToArray()[13],
                    SQ = d.ToArray()[14],
                    PE = d.ToArray()[15],
                    CreateTime = DateTime.Now
                });
            }

            // Get the DataTable 
            DataTable dtInsertRows = db_main.ToDataTable();
            DB.Insert(dtInsertRows, DB.TW_Main);
        }
    }
}
